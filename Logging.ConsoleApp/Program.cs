﻿using System;
using NLog;
using NLog.Targets.ElasticSearch;

namespace Logging.ConsoleApp
{
    class Program
    {
        static void Main()
        {
            var configuration = new NLog.Config.LoggingConfiguration();
            var elasticSearchTarget = new ElasticSearchTarget()
            {
                Name = "elastic",
                Uri = "http://192.168.99.100:9200",
                Index = "TestApplication",
                IncludeAllProperties = true,
                Layout = "${longdate}|${level:uppercase=true}|${logger}|${message:raw=true}"
            };
            configuration.AddTarget(elasticSearchTarget);
            configuration.AddRuleForAllLevels("elastic");
            LogManager.Configuration = configuration;

            var logger = LogManager.GetLogger("Example");

            logger.Info("Structured logging {@number}", 456);

            Console.ReadLine();
        }
    }
}
